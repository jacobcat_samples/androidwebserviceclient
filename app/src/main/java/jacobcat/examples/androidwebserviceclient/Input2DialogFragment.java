package jacobcat.examples.androidwebserviceclient;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import jacobcat.examples.androidwebserviceclient.ws.GetSampleAsyncTask;
import jacobcat.examples.androidwebserviceclient.ws.PostSampleAsyncTask;

/**
 * Created by ultim on 2/8/2016.
 */
public class Input2DialogFragment extends DialogFragment {
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        // Get the layout inflater
        LayoutInflater inflater = getActivity().getLayoutInflater();

        // Set title
        builder.setTitle(getString(R.string.title_dialog));

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        builder.setView(inflater.inflate(R.layout.dialog_input2, null))
                // Add the buttons
                .setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        Activity myActivity = Input2DialogFragment.this.getActivity();
                        EditText inputId = (EditText) ((AlertDialog)dialog).findViewById(R.id.input_id);
                        EditText inputName = (EditText) ((AlertDialog)dialog).findViewById(R.id.input_name);
                        EditText baseUrl = (EditText) myActivity.findViewById(R.id.ws_url);
                        ProgressBar pb = (ProgressBar) myActivity.findViewById(R.id.progressBar);
                        pb.setVisibility(View.VISIBLE);

                        PostSampleAsyncTask.Input params = new PostSampleAsyncTask.Input();
                        params.baseUrl = baseUrl.getText().toString();
                        params.activity = myActivity;
                        params.id = inputId.getText().toString();
                        params.name = inputName.getText().toString();

                        (new PostSampleAsyncTask()).execute(new PostSampleAsyncTask.Input[] { params });

                        dialog.dismiss();
                    }
                })
                .setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.cancel();

                    }
                });

        return builder.create();

    }
}
