package jacobcat.examples.androidwebserviceclient.util;

import android.os.Build;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Scanner;

/**
 * Created by ultim on 2/8/2016.
 */
public class WebServiceUtil {
    public enum WebServiceMethod {
        GET,
        POST
    }

    public static JSONObject callWS(String url, WebServiceMethod method, JSONObject params) throws IOException, JSONException {
        disableConnectionReuseIfNecessary();

        JSONObject result = null;
        HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
        conn.setRequestMethod(method.name());

        try {
            if(params != null) {
                conn.setRequestProperty("Content-Type", "application/json");
                conn.setDoOutput(true);
                conn.setChunkedStreamingMode(0);
                PrintWriter out = new PrintWriter(conn.getOutputStream());
                out.print(params.toString());
                out.close();
            }

            // handle issues
            int statusCode = conn.getResponseCode();
            if (statusCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
                // handle unauthorized (if service requires user login)
                result = new JSONObject("{'status':'error', 'message':'Access denied - requires user login'}");
            } else if (statusCode != HttpURLConnection.HTTP_OK) {
                // handle any other errors, like 404, 500,..
                result = new JSONObject("{'status':'error', 'message':'Error code " + statusCode + " found.'}");
            }
            else {

                InputStream in = new BufferedInputStream(conn.getInputStream());
                result = new JSONObject(getResponseText(in));
                in.close();
            }
        }
        finally {
            conn.disconnect();
        }

        return result;
    }

    private static void disableConnectionReuseIfNecessary() {
        // Work around pre-Froyo bugs in HTTP connection reuse.
        if (Integer.parseInt(Build.VERSION.SDK) < Build.VERSION_CODES.FROYO) {
            System.setProperty("http.keepAlive", "false");

        }}
    private static String getResponseText(InputStream inStream) {
        // very nice trick from
        // http://weblogs.java.net/blog/pat/archive/2004/10/stupid_scanner_1.html
        return new Scanner(inStream).useDelimiter("\\A").next();
    }
}
