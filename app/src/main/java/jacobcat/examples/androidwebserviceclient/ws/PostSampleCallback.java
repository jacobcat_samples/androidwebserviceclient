package jacobcat.examples.androidwebserviceclient.ws;

import org.json.JSONObject;

/**
 * Created by ultim on 2/8/2016.
 */
public interface PostSampleCallback {
    public void onPostSampleSuccessful(JSONObject result);
    public void onPostSampleFailure();
}
