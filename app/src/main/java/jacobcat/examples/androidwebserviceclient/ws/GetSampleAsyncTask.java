package jacobcat.examples.androidwebserviceclient.ws;

import android.app.Activity;
import android.os.AsyncTask;
import jacobcat.examples.androidwebserviceclient.util.WebServiceUtil;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by ultim on 2/8/2016.
 */
public class GetSampleAsyncTask extends AsyncTask<GetSampleAsyncTask.Input, Void, JSONObject> {

    public static class Input {
        public String baseUrl;
        public Activity activity;
        public String id;
    }

    private Activity activity;

    @Override
    protected JSONObject doInBackground(GetSampleAsyncTask.Input... params) {
        this.activity = params[0].activity;
        JSONObject result = null;
        try {
            result = WebServiceUtil.callWS(params[0].baseUrl + "/getsample/" + params[0].id, WebServiceUtil.WebServiceMethod.GET, null);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if(this.activity instanceof GetSampleCallback) {
            if (jsonObject == null) {
                ((GetSampleCallback) this.activity).onGetSampleFailure();
            }
            else {
                ((GetSampleCallback) this.activity).onGetSampleSuccessful(jsonObject);
            }
        }
    }

}
