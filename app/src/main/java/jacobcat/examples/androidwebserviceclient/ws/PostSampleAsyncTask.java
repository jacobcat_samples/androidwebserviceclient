package jacobcat.examples.androidwebserviceclient.ws;

import android.app.Activity;
import android.os.AsyncTask;
import jacobcat.examples.androidwebserviceclient.util.WebServiceUtil;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by ultim on 2/8/2016.
 */
public class PostSampleAsyncTask extends AsyncTask<PostSampleAsyncTask.Input, Void, JSONObject> {

    public static class Input {
        public String baseUrl;
        public Activity activity;
        public String id;
        public String name;
    }

    private Activity activity;

    @Override
    protected JSONObject doInBackground(PostSampleAsyncTask.Input... params) {
        this.activity = params[0].activity;
        JSONObject result = null;
        try {
            JSONObject data = new JSONObject();
            data.put("id", params[0].id);
            data.put("name", params[0].name);
            result = WebServiceUtil.callWS(params[0].baseUrl + "/postsample", WebServiceUtil.WebServiceMethod.POST, data);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if(this.activity instanceof PostSampleCallback) {
            if (jsonObject == null) {
                ((PostSampleCallback) this.activity).onPostSampleFailure();
            }
            else {
                ((PostSampleCallback) this.activity).onPostSampleSuccessful(jsonObject);
            }
        }
    }

}
