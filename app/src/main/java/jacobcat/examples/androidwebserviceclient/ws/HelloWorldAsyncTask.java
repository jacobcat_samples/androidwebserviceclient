package jacobcat.examples.androidwebserviceclient.ws;

import android.app.Activity;
import android.os.AsyncTask;
import android.view.View;
import android.widget.ProgressBar;
import jacobcat.examples.androidwebserviceclient.util.WebServiceUtil;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

/**
 * Created by ultim on 2/8/2016.
 */
public class HelloWorldAsyncTask extends AsyncTask<HelloWorldAsyncTask.Input, Void, JSONObject> {

    public static class Input {
        public String baseUrl;
        public Activity activity;
    }

    private Activity activity;

    @Override
    protected JSONObject doInBackground(HelloWorldAsyncTask.Input... params) {
        this.activity = params[0].activity;
        JSONObject result = null;
        try {
            result = WebServiceUtil.callWS(params[0].baseUrl + "/helloworld", WebServiceUtil.WebServiceMethod.GET, null);
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return result;
    }

    @Override
    protected void onPostExecute(JSONObject jsonObject) {
        super.onPostExecute(jsonObject);

        if(this.activity instanceof HelloWorldCallback) {
            if (jsonObject == null) {
                ((HelloWorldCallback) this.activity).onHelloWorldFailure();
            }
            else {
                ((HelloWorldCallback) this.activity).onHelloWorldSuccessful(jsonObject);
            }
        }
    }

}
