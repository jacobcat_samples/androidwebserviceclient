package jacobcat.examples.androidwebserviceclient.ws;

import org.json.JSONObject;

/**
 * Created by ultim on 2/8/2016.
 */
public interface GetSampleCallback {
    public void onGetSampleSuccessful(JSONObject result);
    public void onGetSampleFailure();
}
