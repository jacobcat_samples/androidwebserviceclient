package jacobcat.examples.androidwebserviceclient;

import android.app.Activity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import jacobcat.examples.androidwebserviceclient.ws.GetSampleCallback;
import jacobcat.examples.androidwebserviceclient.ws.HelloWorldAsyncTask;
import jacobcat.examples.androidwebserviceclient.ws.HelloWorldCallback;
import jacobcat.examples.androidwebserviceclient.ws.PostSampleCallback;
import org.json.JSONObject;

public class MainActivity extends Activity implements HelloWorldCallback, GetSampleCallback, PostSampleCallback {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    public void callHelloWorld(View v) {
        ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar);
        pb.setVisibility(View.VISIBLE);

        EditText baseUrl = (EditText) findViewById(R.id.ws_url);
        HelloWorldAsyncTask.Input param = new HelloWorldAsyncTask.Input();
        param.baseUrl = baseUrl.getText().toString();
        param.activity = this;
        (new HelloWorldAsyncTask()).execute(new HelloWorldAsyncTask.Input[] { param });
    }

    public void callGetSample(View v) {
        InputDialogFragment dlg = new InputDialogFragment();
        dlg.show(getFragmentManager(), "input" );
    }

    public void callPostSample(View v) {
        Input2DialogFragment dlg = new Input2DialogFragment();
        dlg.show(getFragmentManager(), "input2" );
    }

    private void onSuccess(JSONObject result) {
        ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar);
        pb.setVisibility(View.INVISIBLE);

        TextView output = (TextView) findViewById(R.id.ws_output);
        output.setText(result.toString());
        Toast.makeText(this, getText(R.string.msg_success), Toast.LENGTH_LONG).show();
    }

    private void onFailure() {
        ProgressBar pb = (ProgressBar) findViewById(R.id.progressBar);
        pb.setVisibility(View.INVISIBLE);

        TextView output = (TextView) findViewById(R.id.ws_output);
        output.setText("");
        Toast.makeText(this, getText(R.string.msg_failure), Toast.LENGTH_LONG).show();
    }

    @Override
    public void onHelloWorldSuccessful(JSONObject result) {
        onSuccess(result);
    }

    @Override
    public void onHelloWorldFailure() {
        onFailure();
    }

    @Override
    public void onGetSampleSuccessful(JSONObject result) {
        onSuccess(result);
    }

    @Override
    public void onGetSampleFailure() {
        onFailure();
    }


    @Override
    public void onPostSampleSuccessful(JSONObject result) {
        onSuccess(result);
    }

    @Override
    public void onPostSampleFailure() {
        onFailure();
    }
}
